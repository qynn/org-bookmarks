;;; config.el --- org-bookmarks layer configuration file
;;
;; Copyright (C) 2020 Qynn Swaan
;;
;; Author: Qynn Swaan <qynn@riseup.net>
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

(setq org-bookmarks-dir "~/.local/share/org-bookmarks")

(setq org-bookmarks-test-file (concat default-directory "test.org"))

(setq org-bookmarks-capture-template
      "* %? [[%^{url|%(org-bookmarks/retrieve-url)}][%^{title|%(org-bookmarks/retrieve-title)}]]\n\t:PLAIN: %(org-bookmarks/retrieve-url)\n\t:COMMENT:\t%^{Comment}\n\n")


;; optional argument passed to (org-capture-finalize)
;; if non nil, jump to the location of the captured bookmark
(setq org-bookmarks-stay-with-capture t)
