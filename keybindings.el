;;; keybindings.el --- org-bookmarks layer key-bindings file
;;
;; Copyright (C) 2020 Qynn Swaan
;;
;; Author: Qynn Swaan <qynn@riseup.net>
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3


(spacemacs/set-leader-keys "oB" 'org-bookmarks/capture)

(spacemacs/set-leader-keys "oT" 'org-bookmarks/test)

(spacemacs/set-leader-keys "oo" 'insert-org-bookmarks)
