;;; tests.el --- org-bookmarks tests

;; Copyright (C) 2020 Qynn Swaan

;; Author: Qynn Swaan <qynn@riseup.net>
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3


(require 'ert)

(ert-deftest test-keys-from-names ()
  (let ((input '("he" "header" "hell"
                 "f1" "f2" "f"
                 "the" "theses" "thesos" "there" "th"
                 "time" "t" "new" "a" "references" "ref"))
        (output '())
        (expected '("he." "hea" "hel"
                    "f1" "f2" "f."
                    "the." "these" "theso" "ther" "th."
                    "ti" "t." "n" "a" "refe" "ref."))
        )
    (setq output (org-bookmarks/get-keys-from-names input))
    (print output)
    (should
     (equal output expected)
    )
  )
)


(defun test-capture-expansion ()
  (let ((target (concat default-directory "plain.org")))
        (message target)
        (setq org-capture-templates '())
        (setq template `("T" "Test" plain (file ,target) "%^{prompt}\n%\1"))
        )
  (add-to-list 'org-capture-templates template)
  (org-capture)
  )
(test-capture-expansion)


(defun test-capture-template ()
  (let ((target (concat default-directory "plain.org")))
    (setq org-capture-templates '())
    (setq template `("T" "Test" plain (file ,target) ,org-bookmarks-capture-template :empty-lines 1))
    )
  (add-to-list 'org-capture-templates template)
  (org-capture)
  (org-capture-finalize org-bookmarks-stay-with-capture)
  )
(test-capture-template)


(defun test-bookmark-capture ()
  (setq org-capture-templates '())
  (add-to-list 'org-capture-templates '("T" "test file"))
  (let ((key "Te")
        (title "./prefix")
        (prefix))
    (setq prefix `(,key ,title))
    (add-to-list 'org-capture-templates prefix))
  (let ((keys "Tet")
        (path "./prefix/template")
        (node '("Fun" "Helium" "Fun"))
        (template))
    (setq template `(,keys ,path entry (file+olp org-bookmarks-test-file . ,node)
                           ,org-bookmarks-capture-template :empty-lines 1))
    (add-to-list 'org-capture-templates template))
  (org-capture)
  (org-capture-finalize org-bookmarks-stay-with-capture)
  )
(test-bookmark-capture)


(defun test-parse-file ()
  (interactive)
  (setq org-capture-templates '())
  (setq org-capture-templates '())
  (setq templates (org-bookmarks/parse-file org-bookmarks-test-file "T"))
  (org-bookmarks/add-templates templates)
  (org-bookmarks/capture)
  (org-capture-finalize org-bookmarks-stay-with-capture)
  )
(test-parse-file)
