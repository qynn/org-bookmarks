;;; packages.el --- org-bookmarks layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2018 Sylvain Benner & Contributors
;;
;; Author: Qynn Swaan <qynn@riseup.net>
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

(defconst org-bookmarks-packages
  '(
    simpleclip
    org
    org-cliplink
    )
)

(defun org-bookmarks/init-simpleclip ()
    (use-package simpleclip :defer t))

;; (defun org-bookmarks/init-org ()
  ;; (use-package org :defer t))

(defun org-bookmarks/init-org-cliplink ()
  (use-package org-cliplink
    :defer t
    :init
    (progn
      (message "org-cliplink loading")
      )
    :config
    (message "org-cliplink loaded")
    ))
