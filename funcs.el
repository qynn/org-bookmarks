;;; funcs.el --- org-bookmarks layer functions file for Spacemacs.

;; Copyright (C) 2020 Qynn Swaan

;; Author: Qynn Swaan <qynn@riseup.net>
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

(defun org-bookmarks/get-url ()
  "retrieve and validate clipboard content"
  (interactive)
  (let ((url ""))
    (setq url (read-minibuffer "url: " (org-cliplink-clipboard-content)))
  (message "got url: %s" url)
  url))

(defun org-bookmarks/get-title (url)
  "retrieve and validate title "
  ;; (interactive)
  (let ((title ""))
    (setq title (read-string "title: "(org-cliplink-retrieve-title-synchronously
                                           (org-cliplink-clipboard-content))))
    (message "got title: %s" title)
  title))

(defun org-bookmarks/retrieve-url ()
  "retrieve url from clipboard"
  ;; (interactive)
  (let ((url ""))
    (setq url (org-cliplink-clipboard-content))
    (message "got url: %s" url)
    url))

(defun org-bookmarks/retrieve-title ()
  "retrieve url's title from clipboa"
  ;; (interactive)
  (let ((title ""))
    (setq title (org-cliplink-retrieve-title-synchronously (org-bookmarks/retrieve-url)))
    (message "got title: %s" title)
    title))
(org-bookmarks/retrieve-title)

(defun org-bookmarks/capture ()
  "Capture a bookmark with simpleclip content"
  (interactive)
  (let (url title)

    ;; (setq url (read-string "url: " (org-cliplink-clipboard-content)))
    ;; (setq title (read-string "title: "(org-cliplink-retrieve-title-synchronously url)))

    (message "got %s == %s" url title)
    ;; (org-bookmarks/get-title (org-bookmarks/get-url))
    ;; (setq link (read-minibuffer "url: " (simpleclip-get-contents)))
    ;; (message "link: %s" link)
    ;; ;; TODO catch this: %![Error: (void-function simpleclip-get-contents)]

    (org-capture)
    ))


(defun org-bookmarks/get-files-from-dir (dir)
  "Look for all .org files in directory"
  (let ((files (directory-files dir nil ".org$")))
    (if (null files)
      (user-error "no bookmark files found!\nAborting!"))
    (print `("FILES" ,files))
    files))

(defun org-bookmarks/get-leader-names-from-files (files)
  "get base names from files"
  (let ((leader-names '()))
  (dolist (f files)
    (add-to-list 'leader-names (f-base f))
  )
  (setq leader-names (reverse leader-names))
  (print `("LEADERS" ,leader-names))
  leader-names))


(defun org-bookmarks/get-headlines-from-file (file)
  "list headlines in org file, omitting links"
  (let ((headlines '())
        (title nil)
        (level nil))
      (with-temp-buffer
        (insert-file-contents file)
        (org-element-map (org-element-parse-buffer) 'headline
          (lambda (headline) (progn
                          (setq title (org-element-property :raw-value headline))
                          (setq level (org-element-property :level headline))
                        (if (not (eq (string-match "^\\[\\[.*\\]\\[.*\\]\\]$" title) 0))
                            (add-to-list 'headlines (cons title level))
                      ;; (message "skipping org link: %s" title)
                        )))))
        (setq headlines (reverse headlines))
        (print `("HEADLINES" ,headlines))
        headlines))

(defun org-bookmarks/get-links-from-file (file)
  "list links in org file"
  (let ((links'()))
    (with-temp-buffer
      (insert-file-contents file)
      (org-element-map (org-element-parse-buffer) 'link
        (lambda (link) (add-to-list 'links (org-element-property :raw-link link)
                                  ))))
    (print `("LINKS" ,links))
    links))

(defun org-bookmarks/get-capture-paths-from-headlines (headlines)
  (let (( paths '())
        (tree '()))
    (loop for j from 0 for h in headlines do
          (let ((i 0);; reward list index
                (l 0);; header level
                (path '()))
            ;; (add-to-list 'path (car h))
            (setq i j)
            (setq l (cdr h))

            (if (or (= (1+ j) (length headlines))
                        (>= l (cdr (nth (1+ j) headlines))))
                (add-to-list 'path (concat (car h) "~"))
              (add-to-list 'path (car h))
                )

            ;;checking backward to collect higher level headlines
            (while (not (eq (length path) (cdr h)))
              (progn
                ;; going back in list to find headlines
                (setq i (- i 1))
                (if (< i 0)
                    (progn
                      (user-error "org-bookmarks: check tree structure around %s!" h)
                      (return)
                      )
                  )
                (if (< (cdr (nth i headlines)) l)
                    (progn
                      (add-to-list 'path (car (nth i headlines)))
                      (setq l (- l 1))
                      ))
                ))
            (add-to-list 'paths path)
            ;; (setq path '())
            ))
    (setq paths (reverse paths))

    (print `("PATHS" ,paths))
           paths))

(defun org-bookmarks/get-capture-tree-from-paths (paths)
  (let ((tree '()))
  (let ((subs '()))
    ;;getting first level headlines
    (dolist (path paths)
      (if (= (length path) 1)
          ;; (add-to-list 'subs (concat (car path) "/"))
          (add-to-list 'subs (car path))
        ))
    (setq subs (reverse subs))
    ;; (message "level1: %s" subs)
    (add-to-list 'tree (cons "/" subs))
    )
  (loop for i from 0 for path in paths do
       (let ((name "/")
             (level (length path))
             (l 1) ;; forward search level
             (j (1+ i))) ;; forward search index
    (setq l (length (nth j paths)))

    (let ((subs '()))
    (while (> l level)
      (if (= l (1+ level))
        (add-to-list 'subs (nth (1- l) (nth j paths))))
      (setq j (1+ j))
      (setq l (length (nth j paths)))
      )

    (setq subs (reverse subs))
    ;; (message "subs for %s : %s" name subs)
    (if (not (null subs))
        (progn
          (dolist (p path) ;;else add path to tree
            (setq name (concat name p "/")))
          (add-to-list 'tree (cons name subs))
      ))
    )))
  (setq tree (reverse tree))
  (print `("TREE" ,tree))
  tree))

(defun org-bookmarks/get-capture-subkeys-from-tree (tree)
  "extract key sequence associated with tree"
  (let ((subkeyset '()))
    (dolist (branch tree)
      (let (subkeys '())
      ;; (message "\nBRANCH: %s" (car branch))
      ;; (message "SUBS: %s" (cdr branch))
      (setq subkeys (org-bookmarks/get-keys-from-names (cdr branch)))
      ;; (message "SUBKEYS: %s" subkeys)
      (add-to-list 'subkeyset (cons (car branch) subkeys))
      ))
    (setq subkeyset (reverse subkeyset))
    (print `("KEYSET" ,subkeyset))
    subkeyset))

(defun org-bookmarks/create-capture-outline (file leader tree)
  "lay out capture template structure"
  (let ((keyset (org-bookmarks/get-capture-subkeys-from-tree tree))
        (outline '())
        )

    (dolist (branch tree)
      (let ((headkey "")
            (subkeys '())
            (keys '())
            (targets '()))
      (message "\nBRANCH: %s " (car branch))

      ;; getting head name based on path prefix
      (dolist (head (reverse (cdr (butlast (split-string (car branch) "\\/")))))
        ;; (message "head: %s" head)
          (loop for i from 0 for b in tree do
                (loop for j from 0 for h in (cdr b) do
                (if (equal h head)
                      (setq headkey (concat (nth j (cdr (nth i keyset))) "-" headkey))
                ))))
      ;; (message "headkey: %s" headkey)

      ;; (message "SUBS: %s" (cdr branch))
      (setq subkeys (org-bookmarks/get-keys-from-names (cdr branch)))
      ;; (message "SUBKEYS: %s" subkeys)

      (dolist (s subkeys)
        (setq keys (cl-nsubst (concat leader "-" headkey s) s subkeys :test 'equal)))
      ;; (message "KEYS: %s" keys)

        (dolist (p (cdr branch))
          (add-to-list 'targets (concat (f-base file) ": " (car branch) p)))
        ;; (message "TARGETS: %s" targets)
        (add-to-list 'outline (cons (reverse targets) keys))
        ))
    (setq outline (reverse outline))
    ;; (add-to-list 'outline (cons `(,(concat "./" (f-base file))) `(,leader)))
    (add-to-list 'outline (cons `(,(concat (f-base file) ": ")) `(,leader)))
    ;; (add-to-list 'outline (cons `(,(f-base file)) `(,leader)))
    ;; (print `("TREE" ,tree))
    (print `("OUTLINE" ,outline))
    outline))

(defun org-bookmarks/increment-key (key name)
  "add new character to key based on name"
  (let ((j (length key)))
    (if (not (string= (substring key (1- j) j) "."))
        (if (= (length name) j)
            (setq key (concat key "."))
          (setq key (substring name 0 (1+ j)))
          )))
  key)

(defun org-bookmarks/get-keys-from-names (names)
  "Generate a list of minimally unique substrings"
  (let ((keys '()))
    (if (not(eq names (remove-duplicates names :test 'equal)))
        (user-error "found duplicates in %s.\nAborting!" names))
    (dolist (name names)
      (let (
            (key (substring name 0 1))
            (valid nil))
        ;; (message "\n+ name: %s" name)
        (while (not valid)
          (setq valid t) ;; benefit of the doubt
          (dolist (k keys)
            ;; check if key is a prefix of an same key
            (setq valid (and valid (not (string-prefix-p key k))))
            (if (not valid)
                (progn
                  (if (member key keys) ;; exact same key already exists
                      (while (member key keys)
                        (let ((pos "") ;; position of the same key
                              (same "") ;; associated name
                              (new "")) ;; new key for this name
                          (setq pos (cl-position key keys :test 'equal) )
                          (setq same (nth pos names))
                          ;; (message "\t\t key [%s] already at pos %s (%s)" key pos same)
                          (setq new (org-bookmarks/increment-key key same))
                          (cl-nsubst new key keys :test 'equal)
                          (setq key (org-bookmarks/increment-key key name))
                          ;; (message "\t\t updating key to [%s] (%s)" key name)
                          ))
                    (setq key (org-bookmarks/increment-key key name))
                    ;; (message "\t\t adding new char to key: [%s]" key)
                    ) ;; progn
                  (return)
                  ))))
        (add-to-list 'keys key t)
        ;; (message "\t adding key [%s]" key)
        ;; (message "\t keys: %s" keys)
        )) ;; dolist names
    ;; (message "final keys: %s" keys)
    keys))

(defun org-bookmarks/create-capture-templates (file outline)
  "generate capture template associated with outline"
  (let ((templates '()))
    (dolist (branch outline)

      (loop for i from 0 for seq in (cdr branch) do
            (let ((path (nth i (car branch)))
                  (header "")
                  (tail (car (last (split-string seq "\\-"))))
                  (keys (replace-regexp-in-string "\\-" "" seq)))

              (message "\ncreating template(s) for [%s] = %s" keys path)
              (if (equal (substring path -1 nil) "~")
                  (progn
                    (setq path (substring path 0 -1))
                    (setq header (car (last (split-string path "\\/"))))
                    ;; (message "path: %s  header: %s" path header)
                    (setq template `(,keys ,path entry (file+headline ,file ,header) "* %? %(org-cliplink-capture)\n:URL: %(org-bookmarks/get-url) \n:COMMENT: %^{Comment}\n\n" :empty-lines 1))

                    ;; (message "template: %s" template)
                    (add-to-list 'templates template)
                    ;; (add-to-list 'org-capture-templates template)
                    ;; https://orgmode.org/manual/Template-expansion.html
                  )
                (message "keys+path:")
                (print `(,keys ,path))
                (add-to-list 'templates `(,keys ,path))

              ;; checking for intermediate capture paths
                )
              (if (> (length tail) 1)
                  (progn
                    ;; (message "W! will have to split %s" tail)
                    (let ((hint nil)
                          (prefix "")
                          (suffix ""))

                      ;; building hint by looking for other contender headers
                      (dolist (b outline)
                        ;; (dolist (k (cdr b))
                        (loop for j from 0 for k in (cdr b) do
                              (if (equal (substring seq 0 -1) (substring k 0 -1))
                                  (progn
                                    ;; (message "found %s at j:%s in %s" k j b)
                                    (if (null hint)
                                        (setq hint (nth j (car b)))
                                      ;; TODO improve hint appearance
                                      (setq hint (concat hint "|" (car (last (split-string (nth j (car b)) "\\/")))))
                                      )
                                    ))))
                      ;; (message "hint: %s" hint)

                      ;;getting prefix
                      (setq prefix (substring keys 0 (- (length tail))))
                      ;; (message "prefix: %s" prefix)

                      ;; building suffix incrementally
                      (loop for c across (substring tail 0 -1) do
                            (setq suffix (concat suffix (string c)))
                            ;; (message " > > creating template for [%s] = %s" (concat prefix suffix) hint)
                            (add-to-list 'templates `(,(concat prefix suffix) ,hint))

                            ))
                  )))


            ;;TODO check if path is a end path or a headline !!


            ))
      (setq templates (reverse templates))
      ;; (print `("TEMPLATES: " ,templates))
      templates))


(defun org-bookmarks/parse-file (file leader)
  "parse file and generate capture templates"
  (let ((headlines '())
        (paths '())
        (tree '())
        (outline '())
        (templates '()))
    (setq headlines (org-bookmarks/get-headlines-from-file file))
    (setq paths (org-bookmarks/get-capture-paths-from-headlines headlines))
    (setq tree (org-bookmarks/get-capture-tree-from-paths paths))
    (setq outline (org-bookmarks/create-capture-outline file leader tree))
    (setq templates (org-bookmarks/create-capture-templates file outline))

    (print `("TEMPLATES: " ,templates))
    templates))

(defun org-bookmarks/add-templates (templates)
  "concatenate list of templates to org-capture-templates"
  (setq org-capture-templates (nconc templates org-capture-templates))
  org-capture-templates)

(defun org-bookmarks/add-file ()
  "Select new file to parse for capture templates"
  (interactive)
  (let ((file "")
        (leader "")
        (templates '()))
    (setq file (read-file-name "select file: " org-bookmarks-dir))
    (setq leader (read-minibuffer "short key: "))
    ;; TODO make sure key is unique?
    (message "new file %s [%s]" file leader)
    ;; (add-to-list 'org-bookmarks-files file)
    (message "PARSING %s = [%s]" file leader)
    (setq templates (org-bookmarks/parse-file file leader))
    (org-bookmarks/add-templates templates)
    ))

(defun org-bookmarks/init ()
  (interactive)
  (setq org-bookmarks-files (org-bookmarks/get-files-from-dir org-bookmarks-dir))
  (setq org-bookmarks-leader-names (org-bookmarks/get-leader-names-from-files org-bookmarks-files))
  (setq org-bookmarks-leader-keys (org-bookmarks/get-keys-from-names org-bookmarks-leader-names))
  (print `("LEADER KEYS" ,org-bookmarks-leader-keys))
)

  ;; (setq org-bookmarks/parse-files '())
  ;; (loop for i from 0 for f in org-bookmarks-files do
  ;; (let ((leader (nth i org-bookmarkg-leader-names))
  ;;       (file (concat org-bookmarks-dir "/" f))
  ;;       (headlines '())
  ;;       (links '())
  ;;       (paths '())
  ;;       (tree '())
  ;;       (outline '())
  ;;       (templates '()))

  ;;   (message "NOW PARSING %s = [%s]" file leader)
  ;;   (setq headlines (org-bookmarks/get-headlines-from-file file))
  ;;   (setq links (org-bookmarks/get-links-from-file file))
  ;;   (setq paths (org-bookmarks/get-capture-paths-from-headlines headlines))
  ;;   (setq tree (org-bookmarks/get-capture-tree-from-paths paths))
  ;;   (setq outline (org-bookmarks/create-capture-outline file leader tree))
  ;;   (setq templates (org-bookmarks/create-capture-templates file outline))
  ;;   (setq org-bookmarks-templates (nconc templates org-bookmarks-templates))
  ;;   ))
  ;; ;; (print "org-bookmarks-templates:")

  ;; ;; (setq org-capture-templates '())
  ;; (setq org-capture-templates org-bookmarks-templates)
  ;; ;; (print org-capture-templates)

  ;; org-bookmarks-templates)
